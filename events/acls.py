import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    params = {"per_page": 1, "query": city + " " + state}
    pexels_url = "https://api.pexels.com/v1/search"
    response = requests.get(pexels_url, params=params, headers=headers)
    content = json.loads(response.content)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    picture_url = {"picture_url": content["photos"][0]["src"]["original"]}
    return picture_url


# def get_weather(city, state):
#     headers = {"authorization": OPEN_WEATHER_API_KEY}
#     geocode_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},&limit=1&appid={headers}"
#     response = requests.get(geocode_url)
#     geocode = json.loads(response.content)
#     lat = geocode[0]["lat"]
#     lon = geocode[0]["lon"]

#     current_weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={headers}"

#     weather_response = response.get(current_weather_url, headers=headers)

#     weather_data = json.loads(weather_response.content)

#     weather = {
#         "weather_description": weather_data["weather"][0]["description"],
#         "weather_temp": weather_data["main"]["temp"],
#     }
#     return weather


def get_weather_data(city, state):
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None
